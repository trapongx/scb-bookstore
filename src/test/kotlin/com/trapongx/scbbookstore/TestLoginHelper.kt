package com.trapongx.scbbookstore

import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

fun MockMvc.createJohnDoe() {
    perform(
        post("/users")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content("""{"username": "john.doe", "password": "thisismysecret", "name": "john", "surname": "doe", "date_of_birth": "15/01/1985"}""")
    ).andExpect(status().isOk)
}

fun MockMvc.loginWithJohnDoe(assert: Boolean = true) {
    perform(
        post("/login")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content("""{"username": "john.doe", "password":"thisismysecret"}""")
    ).also { if (assert) it.andExpect(status().isOk) }
}

fun MockMvc.logout() = perform(get("/logout"))
