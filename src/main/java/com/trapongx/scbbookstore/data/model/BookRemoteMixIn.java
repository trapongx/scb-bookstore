package com.trapongx.scbbookstore.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class BookRemoteMixIn {
    @JsonProperty("book_name")
    String name;

    @JsonProperty("author_name")
    String author;
}