# Configuration file
The file *'application-xxx.yml'* is used when active profile is *'xxx'*.

If no active profile were set, *'application.yml'* is used by default.

This is the case when developer checkout project and run it without changing any configuration.
