package com.trapongx.scbbookstore.controller

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.trapongx.scbbookstore.DefaultAppContextSharedTestWithSampleBooks
import com.trapongx.scbbookstore.createJohnDoe
import com.trapongx.scbbookstore.data.repo.UserRepository
import com.trapongx.scbbookstore.loginWithJohnDoe
import com.trapongx.scbbookstore.logout
import com.trapongx.util.DateUtil
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.text.SimpleDateFormat
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
@WebAppConfiguration
class UserControllerIntegrationTest : DefaultAppContextSharedTestWithSampleBooks(), ControllerTestWithLogoutAfterTest {

    @Autowired
    lateinit var wac: WebApplicationContext

    override lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var userRepository: UserRepository

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build()
    }

    @Test
    @Throws(Exception::class)
    fun testCreate() {
        Assertions.assertThat(userRepository.findByUsername("john.doe")).isNull()

        mockMvc.perform(
            post("/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""{"username": "john.doe", "password": "thisismysecret", "date_of_birth": "15/01/1985"}""")
        ).andExpect(status().isOk)

        val johnDoe = userRepository.findByUsername("john.doe")

        Assertions.assertThat(johnDoe).isNotNull()
        johnDoe?.also {
            Assertions.assertThat(it.password).isNotNull()
            Assertions.assertThat(it.password!!.length).isEqualTo(60)
            Assertions.assertThat(it.name).isNull()
            Assertions.assertThat(it.surname).isNull()
            Assertions.assertThat(it.dateOfBirth).isEqualTo(SimpleDateFormat("dd/MM/yyyy").parse("15/01/1985"))
        }
    }

    class UserInfo {
        var name: String? = null
        var surname: String? = null
        @JsonProperty("date_of_birth")
        var dateOfBirth: Date? = null
        var books: List<Long>? = null
    }

    @Test
    @Throws(Exception::class)
    fun testGetLoggedInUserInfo() {
        mockMvc.perform(get("/users")).andExpect(status().is4xxClientError)

        mockMvc.createJohnDoe()

        mockMvc.perform(get("/users")).andExpect(status().is4xxClientError)

        mockMvc.loginWithJohnDoe()

        fun getUserInfoAndAssert(vararg expectedBookIdList: Long) {
            mockMvc.perform(get("/users")).andExpect(status().isOk)
                .andReturn().response.contentAsString.also { jsonString ->
                    ObjectMapper().readValue(jsonString, UserInfo::class.java).also { userInfo ->
                        Assertions.assertThat(userInfo.name).isEqualTo("john")
                        Assertions.assertThat(userInfo.surname).isEqualTo("doe")
                        Assertions.assertThat(userInfo.dateOfBirth).isEqualTo(DateUtil.of(1985, 0, 15))
                        Assertions.assertThat(userInfo.books?.toTypedArray()).isEqualTo(expectedBookIdList)
                    }
                }
        }

        fun createOrder(vararg bookIds: Long) {
            mockMvc.perform(
                post("/users/orders")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("""{"orders": [${bookIds.joinToString(", ") { it.toString() }} ]}""")
            ).andExpect(status().isOk)
        }

        createOrder(1, 4)

        getUserInfoAndAssert(1, 4)

        createOrder(2, 8)

        getUserInfoAndAssert(1, 4, 2, 8)

        createOrder(1)

        getUserInfoAndAssert(1, 4, 2, 8, 1)

    }

    @Test
    fun testLogoutAndDelete() {
        fun getUserInfoAndAssert() {
            mockMvc.perform(get("/users")).andExpect(status().isOk)
                .andReturn().response.contentAsString.also { jsonString ->
                ObjectMapper().readValue(jsonString, UserInfo::class.java).also { userInfo ->
                    Assertions.assertThat(userInfo.name).isEqualTo("john")
                    Assertions.assertThat(userInfo.surname).isEqualTo("doe")
                }
            }
        }

        fun getUserInfoAndExpectedError() {
            mockMvc.perform(get("/users")).andExpect(status().is4xxClientError)
        }

        mockMvc.createJohnDoe()
        mockMvc.loginWithJohnDoe(assert = true)
        getUserInfoAndAssert()

        mockMvc.logout()
        getUserInfoAndExpectedError()

        mockMvc.perform(delete("/users")).andExpect(status().is4xxClientError)

        mockMvc.loginWithJohnDoe(assert = true)
        mockMvc.perform(delete("/users")).andExpect(status().isOk)

        Assertions.assertThat(userRepository.findByUsername("john.doe")).isNull()
    }
}