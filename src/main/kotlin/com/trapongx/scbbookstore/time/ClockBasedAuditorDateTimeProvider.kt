package com.trapongx.scbbookstore.time

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.auditing.DateTimeProvider
import org.springframework.stereotype.Component
import java.util.*

@Component
class ClockBasedAuditorDateTimeProvider : DateTimeProvider {
    @Autowired
    lateinit var clockService: ClockService

    override fun getNow(): Calendar = Calendar.getInstance().also {
        it.time = clockService.getDate()
    }
}