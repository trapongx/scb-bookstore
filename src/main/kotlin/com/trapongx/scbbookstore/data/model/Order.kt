package com.trapongx.scbbookstore.data.model

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener::class)
class Order {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne(optional = false)
    var user: User? = null

    /**
     * Store book list with price so that future changes of book price will not affect order price.
     */
    @ElementCollection
    @CollectionTable(name = "order_book_price", joinColumns = [JoinColumn(name = "order_id")])
    @MapKeyClass(Book::class)
    @MapKeyJoinColumn(name = "book_id")
    @Column(name = "price")
    var books: MutableMap<Book, Double> = mutableMapOf()

    @NotNull
    var price: Double? = null

    @CreatedDate
    var createdDate: Date? = null

    @PrePersist
    @PreUpdate
    fun assertPrice() {
        val expectedPrice = books.entries.sumByDouble { it.value }
        require(expectedPrice == price) { "Price value mismatched. Expected $expectedPrice but was $price" }
    }
}