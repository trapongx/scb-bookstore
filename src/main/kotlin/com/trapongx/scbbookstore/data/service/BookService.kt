package com.trapongx.scbbookstore.data.service

import com.trapongx.scbbookstore.SchedulingEnablingCondition
import com.trapongx.scbbookstore.data.model.Book
import com.trapongx.scbbookstore.data.repo.BookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Conditional
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class BookService : ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    lateinit var remoteBookService: RemoteBookService

    @Autowired
    lateinit var bookRepository: BookRepository

    @Autowired
    lateinit var environment: Environment

    companion object {
        var successfullyCalledUpdateBooksAtLeastOnceSinceStart = false
            private set
    }

    /**
     * In real situation, the application might skip error in some book and go on with the rest.
     * But for this sample project, it will throw exception to ensure that there's no bug in the application.
     * One assumption implicitly made is that the list of books returned from remote web service
     * is always be a superset of the previous week. There'll be no book removed from book store.
     * Since it's implicit, no validation against this assumption will be made.
     */
    @Transactional
    fun updateBooks() {
        for (book in remoteBookService.fetch()) {
            when (val existingVersion = bookRepository.findOne(book.id)) {
                null -> bookRepository.save(book)
                else -> {
                    val hasChange = book.name != existingVersion.name
                        || book.author != existingVersion.author
                        || book.price != existingVersion.price
                        || book.isRecommended != existingVersion.isRecommended
                    if (hasChange) {
                        existingVersion.also {
                            it.name = book.name
                            it.author = book.author
                            it.price = book.price
                            it.isRecommended = book.isRecommended
                        }.also {
                            bookRepository.save(it)
                        }
                    }
                }
            }
        }
        successfullyCalledUpdateBooksAtLeastOnceSinceStart = true
    }

    @Component
    @Conditional(SchedulingEnablingCondition::class)
    class Schedule {
        @Autowired lateinit var bookService: BookService

        @Scheduled(cron = "0 0 0 ? * SUN")
        fun trigger() {
            bookService.updateBooks()
        }
    }

    fun getBooksWithRecommendationFirst(): List<Book> {
        val books = bookRepository.findAllByOrderByIsRecommendedDescIdAsc()
        return if (books.isEmpty() && !successfullyCalledUpdateBooksAtLeastOnceSinceStart) {
            updateBooks()
            bookRepository.findAllByOrderByIsRecommendedDescIdAsc()
        } else books
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent?) {
        if (!environment.activeProfiles.contains("integrationTest")) {
            if (bookRepository.count() == 0L) updateBooks()
        }
    }

}