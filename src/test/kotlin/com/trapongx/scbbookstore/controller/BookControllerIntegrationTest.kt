package com.trapongx.scbbookstore.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.trapongx.scbbookstore.DefaultAppContextSharedTest
import com.trapongx.scbbookstore.data.model.Book
import com.trapongx.scbbookstore.data.service.BookService
import com.trapongx.scbbookstore.data.service.RemoteBookService
import com.trapongx.util.getResource
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@RunWith(SpringRunner::class)
@SpringBootTest
@WebAppConfiguration
class BookControllerIntegrationTest : DefaultAppContextSharedTest(), ControllerTestWithLogoutAfterTest {

    @Autowired
    lateinit var wac: WebApplicationContext

    override lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var bookService: BookService

    @Mock
    lateinit var remoteBookUrlProviderMock: RemoteBookService.UrlProvider

    private lateinit var remoteBookUrlProviderSaved: RemoteBookService.UrlProvider

    @Before
    fun initMocks() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build()

        MockitoAnnotations.initMocks(this)
        remoteBookUrlProviderSaved = bookService.remoteBookService.urlProvider
        bookService.remoteBookService.urlProvider = remoteBookUrlProviderMock
    }

    @After
    fun restoreDependencies() {
        bookService.remoteBookService.urlProvider = remoteBookUrlProviderSaved
    }

    @Test
    @Throws(Exception::class)
    fun testGetBooks() {
        val packagePath = "com/trapongx/scbbookstore/data/service"

        Mockito.`when`(remoteBookUrlProviderMock.getBooksUrl())
            .thenReturn(getResource("$packagePath/sample-remote-books-1.json"))
        Mockito.`when`(remoteBookUrlProviderMock.getBooksRecommendationUrl())
            .thenReturn(getResource("$packagePath/sample-remote-books-recommendation-1.json"))

        bookService.updateBooks()

        mockMvc.perform(get("/books").contentType(MediaType.APPLICATION_JSON_UTF8))
            .andReturn()
            .response
            .contentAsString
            .also { json ->
                val books = ObjectMapper().readValue<List<Book>>(json, object : TypeReference<List<Book>>(){})
                val expectedBookIds = arrayOf<Long>(4,5,1,2,3,6,7,8,9,10,11,12)
                Assertions.assertThat(books.map { it.id }.toTypedArray()).isEqualTo(expectedBookIds)

                //Simply pick one sample of book to test in detail
                books.find { it.id == 2L }!!
                    .also { book ->
                        Assertions.assertThat(book.name).isEqualTo("When Never Comes")
                        Assertions.assertThat(book.author).isEqualTo("Barbara Davis")
                        Assertions.assertThat(book.price).isEqualTo(179.0)
                    }
            }
    }

}