package com.trapongx.scbbookstore.data.service

import com.trapongx.scbbookstore.DefaultAppContextSharedTest
import com.trapongx.scbbookstore.data.repo.BookRepository
import com.trapongx.util.getResource
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class BookServiceIntegrationTest : DefaultAppContextSharedTest() {

    @Autowired
    lateinit var bookService: BookService

    @Autowired
    lateinit var bookRepository: BookRepository

    @Mock
    lateinit var remoteBookUrlProviderMock: RemoteBookService.UrlProvider

    private lateinit var remoteBookUrlProviderSaved: RemoteBookService.UrlProvider

    @Before
    fun initMocks() {
        MockitoAnnotations.initMocks(this)
        remoteBookUrlProviderSaved = bookService.remoteBookService.urlProvider
        bookService.remoteBookService.urlProvider = remoteBookUrlProviderMock
    }

    @After
    fun restoreDependencies() {
        bookService.remoteBookService.urlProvider = remoteBookUrlProviderSaved
    }

    @Test
    fun testUpdateBooks() {
        val packagePath = "com/trapongx/scbbookstore/data/service"

        `when`(remoteBookUrlProviderMock.getBooksUrl())
            .thenReturn(getResource("$packagePath/sample-remote-books-1.json"))
            .thenReturn(getResource("$packagePath/sample-remote-books-2.json"))
        `when`(remoteBookUrlProviderMock.getBooksRecommendationUrl())
            .thenReturn(getResource("$packagePath/sample-remote-books-recommendation-1.json"))
            .thenReturn(getResource("$packagePath/sample-remote-books-recommendation-2.json"))

        bookService.updateBooks()

        Assertions.assertThat(bookRepository.count()).isEqualTo(12)

        bookRepository.findOne(1)!!.also {
            Assertions.assertThat(it.name).isEqualTo("Before We Were Yours: A Novel")
            Assertions.assertThat(it.author).isEqualTo("Lisa Wingate")
            Assertions.assertThat(it.price).isEqualTo(340.0)
        }

        bookRepository.findOne(4)!!.also {
            Assertions.assertThat(it.isRecommended).isTrue()
        }

        bookRepository.findOne(5)!!.also {
            Assertions.assertThat(it.isRecommended).isTrue()
        }

        bookService.updateBooks()

        Assertions.assertThat(bookRepository.count()).isEqualTo(12)

        bookRepository.findOne(1)!!.also {
            Assertions.assertThat(it.name).isEqualTo("Before We Were Yours: A Novel")
            Assertions.assertThat(it.author).isEqualTo("Lisa Wingate")
            Assertions.assertThat(it.price).isEqualTo(355.0)
        }

        bookRepository.findOne(4)!!.also {
            Assertions.assertThat(it.isRecommended).isFalse()
        }

        bookRepository.findOne(5)!!.also {
            Assertions.assertThat(it.isRecommended).isTrue()
        }
    }
}