package com.trapongx.scbbookstore

import com.trapongx.util.containsAny
import org.springframework.context.annotation.Condition
import org.springframework.context.annotation.ConditionContext
import org.springframework.core.type.AnnotatedTypeMetadata

class SchedulingEnablingCondition : Condition {
    override fun matches(context: ConditionContext?, metadata: AnnotatedTypeMetadata?): Boolean {
        return !context!!.environment.activeProfiles.containsAny("integrationTest", "noScheduling")
    }
}