package com.trapongx.scbbookstore.data.service

import com.trapongx.scbbookstore.DefaultAppContextSharedTest
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class RemoteBookServiceIntegrationTest : DefaultAppContextSharedTest() {

    @Autowired
    lateinit var remoteBookService: RemoteBookService

    @Test
    fun testFetch() {
        remoteBookService.fetch()
    }
}