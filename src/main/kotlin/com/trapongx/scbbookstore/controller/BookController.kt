package com.trapongx.scbbookstore.controller

import com.trapongx.scbbookstore.data.model.Book
import com.trapongx.scbbookstore.data.service.BookService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/books")
class BookController {

    @Autowired
    lateinit var bookService: BookService

    @GetMapping
    fun getBooks(): ResponseEntity<List<Book>> {
        return try {
            ResponseEntity(bookService.getBooksWithRecommendationFirst(), HttpStatus.OK)
        } catch (t: Throwable) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

}