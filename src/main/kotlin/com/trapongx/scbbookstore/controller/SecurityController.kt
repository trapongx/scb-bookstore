package com.trapongx.scbbookstore.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping
class SecurityController {

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    class LoginRequestBody {
        var username: String? = null
        var password: String? = null
    }

    @PostMapping("/login")
    fun login(@RequestBody body: LoginRequestBody): ResponseEntity<Unit> {
        return try {
            SecurityContextHolder.getContext().authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(body.username, body.password)
            )
            ResponseEntity(HttpStatus.OK)
        } catch (bc: BadCredentialsException) {
            ResponseEntity(HttpStatus.UNAUTHORIZED)
        } catch (t: Throwable) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/logout")
    fun logout(request: HttpServletRequest, response: HttpServletResponse): ResponseEntity<Unit> {
        return try {
            val auth = SecurityContextHolder.getContext()?.authentication
            if (auth != null) {
                SecurityContextLogoutHandler().logout(request, response, auth)
                SecurityContextHolder.getContext().authentication = null
            }
            ResponseEntity(HttpStatus.OK)
        } catch (t: Throwable) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

}