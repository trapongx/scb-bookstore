package com.trapongx.scbbookstore.data.repo

import com.trapongx.scbbookstore.data.model.Order
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderRepository : CrudRepository<Order, Long> {
    fun findAllByUserUsernameOrderById(username: String): List<Order>
}