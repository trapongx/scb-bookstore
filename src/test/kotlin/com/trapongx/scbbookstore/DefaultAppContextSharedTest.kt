package com.trapongx.scbbookstore

import com.trapongx.scbbookstore.time.ClockProvider
import com.trapongx.scbbookstore.time.ClockService
import com.trapongx.test.IntegrationTest
import com.trapongx.util.DateUtil
import org.junit.After
import org.junit.Before
import org.junit.experimental.categories.Category
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.time.Clock
import java.time.ZoneId
import java.util.*

/**
 * If signature of application context used for testing is the same, Spring will reuse existing context thus reduce
 * bootstrapping time for the second test on. To have this benefit, let your test classes extend this base class.
 */
@RunWith(SpringRunner::class)
@SpringBootTest
@ActiveProfiles("integrationTest")
@Category(IntegrationTest::class)
@Transactional
@Rollback
abstract class DefaultAppContextSharedTest(val mockClock: Boolean = false) : ApplicationContextAware {
    private var _applicationContext: ApplicationContext? = null

    val applicationContext
        get() = _applicationContext

    override fun setApplicationContext(applicationContext: ApplicationContext?) {
        this._applicationContext = applicationContext!!
    }

    @Autowired
    lateinit var clockService: ClockService

    @MockBean
    lateinit var clockProvider: ClockProvider

    private lateinit var oldClockProvider: ClockProvider

    @Before
    fun mockClock() {
        if (mockClock) {
            oldClockProvider = clockService.provider
            clockService.provider = clockProvider
            timeTravelTo(2018, 0, 1)
        }
    }

    @After
    fun restoreClock() {
        if (mockClock) {
            clockService.provider = oldClockProvider
        }
    }

    fun timeTravelTo(date: Date) {
        Mockito.`when`(clockProvider.getClock()).thenReturn(
                Clock.fixed(date.toInstant(), ZoneId.systemDefault())
        )
    }

    fun timeTravelTo(y: Int, m: Int, d: Int) {
        timeTravelTo(DateUtil.of(y, m, d))
    }

    fun timeTravelTo(y: Int, m: Int, d: Int, h: Int, M: Int, s: Int, ss: Int) {
        timeTravelTo(DateUtil.of(y, m, d, h, M, s, ss))
    }

}
