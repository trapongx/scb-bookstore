package com.trapongx.scbbookstore.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.trapongx.scbbookstore.DefaultAppContextSharedTestWithSampleBooks
import com.trapongx.scbbookstore.createJohnDoe
import com.trapongx.scbbookstore.data.repo.OrderRepository
import com.trapongx.scbbookstore.loginWithJohnDoe
import com.trapongx.util.DateUtil
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@RunWith(SpringRunner::class)
@SpringBootTest
@WebAppConfiguration
class OrderControllerIntegrationTest : DefaultAppContextSharedTestWithSampleBooks(mockClock = true), ControllerTestWithLogoutAfterTest {

    @Autowired
    lateinit var wac: WebApplicationContext

    override lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var orderRepository: OrderRepository

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build()
    }

    class ResponseBody { var price: Double? = null }

    @Test
    @Throws(Exception::class)
    fun testCreate() {
        mockMvc.createJohnDoe()
        mockMvc.loginWithJohnDoe()

        val time = DateUtil.of(2019, 5, 7, 13, 15, 18, 344)

        timeTravelTo(time)

        mockMvc.perform(
            post("/users/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""{"orders": [1, 4]}""")
        )
            .andExpect(status().isOk)
            .andReturn()
            .response
            .contentAsString
            .also { jsonString ->
                ObjectMapper().readValue(jsonString, ResponseBody::class.java).also {
                    Assertions.assertThat(it.price).isEqualTo(835.0)
                }
            }

        orderRepository.findAll().single {
            it.user!!.username == "john.doe"
        }.also {
            Assertions.assertThat(it.books.keys.map { it.id }.toTypedArray()).isEqualTo(arrayOf<Long>(1, 4))
            Assertions.assertThat(it.price).isEqualTo(835.0)
            Assertions.assertThat(it.createdDate).isEqualTo(time)
        }

        mockMvc.perform(
            post("/users/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""{"orders": [101, 104]}""")
        ).andExpect(status().is4xxClientError)
    }

}