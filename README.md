# Language Guide
This Spring Boot application implement mainly in Kotlin.
It could be ported to Java language if needed.
# Configuration
Currently, there're 2 YML configuration provided. 
1. application.yml for default profile
2. application-integrationTest.yml for integration test

# Deployment
## Production
A new configuration file dedicated for production environment must be created and committed into GIT repository. 
The name will be application-production.yml or application-production.properties.

## Development
For each of any developer added to the project, it is good practice to have each guy have their own configuration per machine.
To do this, in case developer name is John, for example, he should create configuration file application-john.yml. 
Then, when start Spring Boot application for his own testing, he must set active profiles to include `john`. 
May be by adding this to JVM argument.
    
    -Dspring.profiles.active=john
    
# Swagger UI available
Swagger2 UI has been added to this project and is available at 
    http://localhost:8080/swagger-ui.html.
Please note that hostname, port and URI prefix might be differ depending on your deployment environment.

# Guide on Spring JPA
This project make use of Spring JPA which come with CriteriaBuilder for ease of query creation.
Anyway, if the data model and the querying logic become complicated. 
It is required to ensure optimal query for the sake of performance but it might not be easy to do so with built-in CriteriaBuilder.
To cope with this problem, let take a look at BlazePersistence library since it introduce more flexible and effective query projection.

# Assumption Made on External Web Service
It return the list of books that always be a superset of what it returned in the past. 
Which means, if any book has been included in the previous request, it will be always included in every request in the future.
If this assumption is incorrect, the book absent will just not get removed or even touched by the updating logic. 
If product owner explicitly stated otherwise, the updating logic must be enhanced to mark some book obsolete. 
Note that deleting of book seems not to be a good solution because some might have been referenced by orders made. 

# Improvement Possibility 
In real world, list of books will grow big. Fetching the whole list of books every time will yield performance penalty.
Architectural design should use incremental update to get only difference between this fetch time and previous fetch time.
Book information might have versioning so that old book name could be known to the application when old orders are getting viewed.

# Unit Testing
Unit test are implemented by using mocking ability provided by Spring and Mockito.
## DefaultAppContextSharedTest
To add new integration test class, please extends it from base DefaultAppContextSharedTest class.
Because each test class has its own configuration and Spring try to optimize testing time by not starting new Spring context
but reuse the same on for test classes that have the same configuration. Extending from DefaultAppContextSharedTest is one way
to make them have the same context configuration.
## Chronological Test Cases with Time Travelling
For test class that make test of the logic that the result are affected by time,
it will be easy if you implement those logic being tested by let it depend on DateTimeProvider instead of using system time directly.
If you did so, then you can control time during test by using timeTravel methods built into the DefaultAppContextSharedTest.

# Adding UML Diagram to The Project
When time is affordable, adding some UML diagram to explain the system and design is a good thing to do.
For IntelliJ IDE user, there's a plugin to display PlantUML file format (*.puml file).
PlantUML allow us to create UML diagram using markdown syntax. It's free and convenient.
See more information at http://plantuml.com/.   