package com.trapongx.scbbookstore.data.repo

import com.trapongx.scbbookstore.data.model.Book
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository : CrudRepository<Book, Long> {
    fun findAllByOrderByIsRecommendedDescIdAsc(): List<Book>
}