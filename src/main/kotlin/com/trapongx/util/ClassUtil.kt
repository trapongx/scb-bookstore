package com.trapongx.util

import java.io.InputStream
import java.net.URL

fun Any.loadResource(name: String): InputStream? = javaClass.classLoader.getResourceAsStream(name)
fun Any.getResource(name: String): URL? = javaClass.classLoader.getResource(name)