package com.trapongx.util

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

val ThaiLocale = Locale("TH", "th")

val MsecForEndOfDay = 999//For MySQL, if column type is DATETIME, this is 0 but if  column type is DATETIME(6), this is 999

object DateUtil {
    fun ofMonth(year: Int, month: Int): Date = of (year, month, 1)

    fun of(year: Int, month: Int, date: Int): Date = Calendar.getInstance().let {
        it.set(year, month, date)
        it.setTimeInDay(0, 0, 0, 0)
        it.time
    }

    fun of(year: Int, month: Int, date: Int, hour: Int, minute: Int, second: Int, millisecond: Int): Date = Calendar.getInstance().let {
        it.set(year, month, date)
        it.setTimeInDay(hour, minute, second, millisecond)
        it.time
    }

    fun monthsBetween(y1: Int, m1: Int, y2: Int, m2: Int): Int {
        if (y1 < 0 || y2 < 0 || m1 < 0 || m1 > 11 || m2 < 0 || m2 > 11)
            throw Exception("Invalid argument")
        return ((y2 - y1) * 12) + (m2 - m1)
    }

    fun rangeOverlapped(firstDate1: Date, lastDate1: Date, firstDate2: Date, lastDate2: Date): Boolean {
        return firstDate1.isBetween(firstDate2, lastDate2)
            || lastDate1.isBetween(firstDate2, lastDate2)
            || firstDate2.isBetween(firstDate1, lastDate1)
    }
}

fun Calendar.setTimeInDay(hour: Int, minute: Int, second: Int, millisecond: Int) {
    set(Calendar.HOUR_OF_DAY, hour)
    set(Calendar.MINUTE, minute)
    set(Calendar.SECOND, second)
    set(Calendar.MILLISECOND, millisecond)
}

fun Date.begin(): Date = GregorianCalendar().let {
    it.time = this
    it.setTimeInDay(0, 0, 0, 0)
    it.time
}

fun Date.end(): Date = GregorianCalendar().let {
    it.time = this
    it.setTimeInDay(23, 59, 59, MsecForEndOfDay)
    it.time
}

fun Date.noTime(): Date = this.begin()

fun Date.sameDayAs(another: Date) = this.noTime() == another.noTime()

fun Date.sameMonthAs(another: Date) = this.sameYearAs(another) && this.getMonthOfYear() == another.getMonthOfYear()

fun Date.sameHalfMonthAs(another: Date) = this.sameYearAs(another) && this.getMonthOfYear() == another.getMonthOfYear()
    && Pair(this.getDayOfMonth(), another.getDayOfMonth()).let {
        (it.first <= 15 && it.second <= 15) || (it.first > 15 && it.second > 15)
    }

fun Date.sameYearAs(another: Date) = this.getYearNumber() == another.getYearNumber()

fun Date.isFuture() = this.noTime().after(Date().noTime())

fun Date.isPast() = this.noTime().before(Date().noTime())

fun Date.daysAfter(another: Date): Int = (this.noTime().time - another.noTime().time).let {
    if (it == 0L) 0 else it.div(24*60*60*1000).toInt()
}

fun Date.daysBefore(another: Date): Int = another.daysAfter(this)

fun Date.addDays(n: Int): Date = GregorianCalendar().also {
    it.time = this
    it.add(Calendar.DATE, n)
}.time

fun Date.beginOfMonth(): Date = GregorianCalendar().let {
    it.time = this
    it.set(Calendar.DAY_OF_MONTH, it.getActualMinimum(Calendar.DAY_OF_MONTH))
    it.setTimeInDay(0, 0, 0, 0)
    it.time
}

fun Date.endOfMonth(): Date = GregorianCalendar().let {
    it.time = this
    it.set(Calendar.DAY_OF_MONTH, it.getActualMaximum(Calendar.DAY_OF_MONTH))
    it.setTimeInDay(23, 59, 59, MsecForEndOfDay)
    it.time
}

fun Date.beginOfHalfMonth(): Date = GregorianCalendar().let {
    it.time = this
    val d = it.get(Calendar.DAY_OF_MONTH).let {
        if (it <= 15) 1 else 16
    }
    it.set(Calendar.DAY_OF_MONTH, d)
    it.setTimeInDay(0, 0, 0, 0)
    it.time
}

fun Date.endOfHalfMonth(): Date = GregorianCalendar().let {
    it.time = this
    val d = it.get(Calendar.DAY_OF_MONTH).let { n ->
        if (n <= 15) 15 else it.getActualMaximum(Calendar.DAY_OF_MONTH)
    }
    it.set(Calendar.DAY_OF_MONTH, d)
    it.setTimeInDay(23, 59, 59, MsecForEndOfDay)
    it.time
}

val ThaiMonthNames: Array<String> = arrayOf(
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน",
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฎาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤศจิกายน",
    "ธันวาคม"
)

val EnglishMonthNames: Array<String> = SimpleDateFormat("MMMMM").let { dfmt ->
    val cal = GregorianCalendar().also { it.set(Calendar.DAY_OF_MONTH, 1) }
    (0..11).map {
        cal.set(Calendar.MONTH, it)
        dfmt.format(cal.time)
    }.toTypedArray()
}

fun String.replaceEnglishMonthWithThaiMonth(greedy: Boolean = false): String {
    var s = this
    for (i in 0..EnglishMonthNames.size) {
        if (this.indexOf(EnglishMonthNames[i]) >= 0) {
            s = s.replace(EnglishMonthNames[i], ThaiMonthNames[i])
            if (!greedy) return s
        }
    }
    return s
}

val defaultDateFormat = SimpleDateFormat("YYYY-MM-dd")
val defaultDateTimeFormat = SimpleDateFormat("YYYY-MM-dd HH:mm:ss")
fun String.toDate() = defaultDateFormat.parse(this)!!
fun String.toDateTime() = defaultDateTimeFormat.parse(this)!!


fun Date.beginOfWeek(): Date = GregorianCalendar().let {
    it.time = this
    it.set(Calendar.DAY_OF_WEEK, it.getActualMinimum(Calendar.DAY_OF_WEEK))
    it.setTimeInDay(0, 0, 0, 0)
    it.time
}

fun Date.endOfWeek(): Date = GregorianCalendar().let {
    it.time = this
    it.set(Calendar.DAY_OF_WEEK, it.getActualMaximum(Calendar.DAY_OF_WEEK))
    it.setTimeInDay(23, 59, 59, MsecForEndOfDay)
    it.time
}

fun Date.beginOfYear(): Date = GregorianCalendar().let {
    it.time = this
    it.set(Calendar.DAY_OF_YEAR, it.getActualMinimum(Calendar.DAY_OF_YEAR))
    it.setTimeInDay(0, 0, 0, 0)
    it.time
}

fun Date.endOfYear(): Date = GregorianCalendar().let {
    it.time = this
    it.set(Calendar.DAY_OF_YEAR, it.getActualMaximum(Calendar.DAY_OF_YEAR))
    it.setTimeInDay(23, 59, 59, MsecForEndOfDay)
    it.time
}

fun Date.getNumberOfDaysInMonth(): Int = GregorianCalendar().let {
    it.time = this
    return it.getActualMaximum(Calendar.DAY_OF_MONTH)
}

fun Date.getNumberOfDaysInYear(): Int = GregorianCalendar().let {
    it.time = this
    return it.getActualMaximum(Calendar.DAY_OF_YEAR)
}

fun Date.getDayOfWeek(): Int = GregorianCalendar().let {
    it.time = this
    return it.get(Calendar.DAY_OF_WEEK)
}

fun Date.getDayOfMonth(): Int = GregorianCalendar().let {
    it.time = this
    return it.get(Calendar.DAY_OF_MONTH)
}

fun Date.getMonthOfYear(): Int = GregorianCalendar().let {
    it.time = this
    return it.get(Calendar.MONTH)
}

fun Date.getYearNumber(): Int = GregorianCalendar().let {
    it.time = this
    return it.get(Calendar.YEAR)
}

fun Date.extractYearMonthDate(): Triple<Int, Int, Int> = GregorianCalendar().let {
    it.time = this
    return Triple(it.get(Calendar.YEAR), it.get(Calendar.MONTH), it.get(Calendar.DAY_OF_MONTH))
}

fun Date.addMonthsCompromiseLastDay(n: Int): Date = GregorianCalendar().also {
    val d = this.getDayOfMonth()
    it.time = this
    it.set(Calendar.DATE, 1)
    it.add(Calendar.MONTH, n)
    val max = it.getActualMaximum(Calendar.DATE)
    it.set(Calendar.DATE, Integer.min(max, d))
}.time

fun Date.addYearsCompromiseLastDay(n: Int): Date = GregorianCalendar().also {
    val d = this.getDayOfMonth()
    it.time = this
    it.set(Calendar.DATE, 1)
    it.add(Calendar.YEAR, n)
    val max = it.getActualMaximum(Calendar.DATE)
    it.set(Calendar.DATE, Integer.min(max, d))
}.time

fun Date.isBetween(d1: Date, d2: Date): Boolean = !this.before(d1) && !this.after(d2)

fun Date.isBetweenOpenRange(d1: Date?, d2: Date?): Boolean = when {
    d1 == null && d2 == null -> true
    d1 != null && d2 != null && this.isBetween(d1, d2) -> true
    d1 != null -> !this.before(d1)
    else -> !this.after(d2)
}

fun Date.endOfMonthsPeriod(months: Int): Date = this.addMonthsCompromiseLastDay(months).let {
    (
        if (it.getDayOfMonth() == this.getDayOfMonth())
            it.addDays(-1)
        else
            it
    ).end()
}

fun Date.endOfOneMonthPeriod(): Date = this.endOfMonthsPeriod(1)

fun Date.endOfYearsPeriod(years: Int): Date = this.addYearsCompromiseLastDay(years).let {
    (
        if (it.getDayOfMonth() == this.getDayOfMonth())
            it.addDays(-1)
        else
            it
    ).end()
}

fun Date.endOfOneYearPeriod(): Date = this.endOfYearsPeriod(1)

fun Date.toLocalDate(): LocalDate = this.extractYearMonthDate().let { LocalDate.of(it.first, it.second, it.third) }

fun Date.toLocalDateTime(): LocalDateTime {
    return GregorianCalendar().let {
        it.time = this
        LocalDateTime.of(
            it.get(Calendar.YEAR),
            it.get(Calendar.MONTH),
            it.get(Calendar.DAY_OF_MONTH),
            it.get(Calendar.HOUR_OF_DAY),
            it.get(Calendar.MINUTE),
            it.get(Calendar.SECOND),
            it.get(Calendar.MILLISECOND)
        )
    }
}
