package com.trapongx.util

import java.lang.reflect.UndeclaredThrowableException

fun Throwable.smartGetMessage(): String {
    return when {
        this is UndeclaredThrowableException -> this.undeclaredThrowable.smartGetMessage()
        this.cause != null && this.cause !== this -> this.cause!!.smartGetMessage()
        this.message != null -> this.message!!
        else -> this.javaClass.name
    }
}