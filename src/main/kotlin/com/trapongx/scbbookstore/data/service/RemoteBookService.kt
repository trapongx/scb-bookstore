package com.trapongx.scbbookstore.data.service

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.trapongx.scbbookstore.data.model.Book
import com.trapongx.scbbookstore.data.model.BookRemoteMixIn
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Service
import java.net.URL

@Service
@ConfigurationProperties(prefix = "remoteBooks")
class RemoteBookService {
    lateinit var endpointUrl: String

    interface UrlProvider {
        fun getBooksUrl(): URL
        fun getBooksRecommendationUrl(): URL
    }

    var urlProvider = object : UrlProvider {
        override fun getBooksUrl() = URL(endpointUrl)

        override fun getBooksRecommendationUrl() = URL("$endpointUrl/recommendation")
    }

    fun fetch(): List<Book> {
        fun fetch(url: URL): List<Book> {
            return ObjectMapper()
                .also { it.addMixIn(Book::class.java, BookRemoteMixIn::class.java) }
                .readValue<List<Book>>(url, object : TypeReference<List<Book>>(){})
        }
        val all = fetch(urlProvider.getBooksUrl())
        val recommendation = fetch(urlProvider.getBooksRecommendationUrl())
        val recommendationIds = recommendation.map { it.id }
        val merged = all.plus(recommendation).distinctBy { it.id }
        merged.forEach { book -> if (book.id in recommendationIds) book.isRecommended = true }
        return merged
    }
}