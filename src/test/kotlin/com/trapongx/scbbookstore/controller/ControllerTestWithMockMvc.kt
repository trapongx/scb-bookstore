package com.trapongx.scbbookstore.controller

import com.trapongx.scbbookstore.logout
import org.junit.After
import org.springframework.test.web.servlet.MockMvc

interface ControllerTestWithLogoutAfterTest {
    val mockMvc: MockMvc

    @After
    fun logoutAfterTest() {
        mockMvc.logout()
    }
}