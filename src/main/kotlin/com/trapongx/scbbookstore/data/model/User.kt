package com.trapongx.scbbookstore.data.model

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.NotBlank
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@Entity
class User {
    @Id
    @GeneratedValue
    var id: Long? = null

    @NotBlank
    @Size(min = 8, max = 30)
    @Pattern(regexp = "[a-zA-Z][a-zA-Z_0-9.@]*")
    @Column(unique = true)
    var username: String? = null

    /**
     * With current design, ciphered password is at 60 characters length.
     * But column capacity is set to 255 characters in order to cope with any change in the future.
     */
    @NotBlank
    @Size(max = 255)
    var password: String? = null

    @Size(max = 100)
    var name: String? = null

    @Size(max = 100)
    var surname: String? = null

    /**
     * Without specifying timezone in @JsonFormat, it will use UTC which yield 7 hours different from actual time.
     * Anyhow, setting it to 'Asia/Bangkok' is not a clean solution.
     * TODO : Create and use custom date deserializer to solve timezone problem instead.
     */
    @JsonProperty("date_of_birth")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Bangkok")
    var dateOfBirth: Date? = null

    @PrePersist
    @PreUpdate
    private fun prePersist() {
        if (password!!.length != 60)
            password = BCryptPasswordEncoder().encode(password)
    }
}