package com.trapongx.scbbookstore.data.service

import com.trapongx.scbbookstore.data.model.ScbBookstoreUserDetails
import com.trapongx.scbbookstore.data.repo.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService : UserDetailsService {

    @Autowired
    lateinit var userRepository: UserRepository

    val passwordEncoder = BCryptPasswordEncoder()

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        return userRepository.findByUsername(username)
            ?.let { ScbBookstoreUserDetails(it) }
            ?: throw UsernameNotFoundException("Cannot find user with specified username and password")
    }

}