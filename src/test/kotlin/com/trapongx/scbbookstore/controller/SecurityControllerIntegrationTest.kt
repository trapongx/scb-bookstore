package com.trapongx.scbbookstore.controller

import com.trapongx.scbbookstore.DefaultAppContextSharedTest
import com.trapongx.scbbookstore.data.model.User
import com.trapongx.scbbookstore.data.repo.UserRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@RunWith(SpringRunner::class)
@SpringBootTest
@WebAppConfiguration
class SecurityControllerIntegrationTest : DefaultAppContextSharedTest(), ControllerTestWithLogoutAfterTest {

    @Autowired
    lateinit var wac: WebApplicationContext

    override lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var userRepository: UserRepository

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build()
    }

    @Test
    @Throws(Exception::class)
    fun testLogin() {
        val johnDoe = User().also {
            it.username = "john.doe"
            it.password = "thisismysecret"
        }.let { userRepository.save(it) }

        fun loginWithJohnDoe(password: String) = mockMvc.perform(
            post("/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""{"username": "john.doe", "password": "$password"}""")
        )

        fun loginWithIncorrectPassword() = loginWithJohnDoe("12345678")

        loginWithIncorrectPassword().andExpect(status().is4xxClientError)

        fun loginWithCorrectPassword() = loginWithJohnDoe("thisismysecret")

        loginWithCorrectPassword().andExpect(status().isOk)

        userRepository.delete(johnDoe)

        loginWithCorrectPassword().andExpect(status().is4xxClientError)
    }

}