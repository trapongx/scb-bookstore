package com.trapongx.scbbookstore.data.model

import org.hibernate.validator.constraints.NotBlank
import javax.persistence.Entity
import javax.persistence.Id
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
class Book {
    @Id
    var id: Long? = null

    @NotBlank
    @Size(max = 255)
    var name: String? = null

    @Size(max = 255)
    var author: String? = null

    @NotNull
    @Min(0)
    var price: Double? = null

    var isRecommended: Boolean = false
        @JvmName("getIsRecommended") get
        @JvmName("setIsRecommended") set
}