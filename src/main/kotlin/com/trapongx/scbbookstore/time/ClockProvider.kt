package com.trapongx.scbbookstore.time

import java.time.Clock

interface ClockProvider {
    fun getClock(): Clock
}