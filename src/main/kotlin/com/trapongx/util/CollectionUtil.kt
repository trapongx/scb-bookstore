package com.trapongx.util

fun <T> Collection<T>.containsAny(vararg elements: T): Boolean = elements.any { this.contains(it) }

fun <T> Array<T>.containsAny(vararg elements: T): Boolean = elements.any { this.contains(it) }

fun <T> Array<T>.prependNull(): List<T?> = this.prepend(null)

fun <T> Array<T>.prepend(v: T?): List<T?> = mutableListOf<T?>().also {
    it.add(v)
    it.addAll(this)
}

fun <T> Iterable<T>.prependNull(): List<T?> = this.prepend(null)

fun <T> Iterable<T>.prepend(v: T?): List<T?> = mutableListOf<T?>().also {
    it.add(v)
    it.addAll(this)
}

fun <T> Array<T>.appendNull(): List<T?> = this.append(null)

fun <T> Array<T>.append(v: T?): List<T?> = mutableListOf<T?>().also {
    it.addAll(this)
    it.add(v)
}

fun <T> Iterable<T>.appendNull(): List<T?> = this.append(null)

fun <T> Iterable<T>.append(v: T?): List<T?> = mutableListOf<T?>().also {
    it.addAll(this)
    it.add(v)
}

fun <T> Collection<T>?.isNullOrEmpty(): Boolean = this?.isEmpty() ?: true
fun <T> Collection<T>?.isNotNullAndNotEmpty(): Boolean = this?.isNotEmpty() ?: false

fun <T> MutableList<T>?.ensureInit(): MutableList<T> = this ?: mutableListOf()
fun <T> MutableSet<T>?.ensureInit(): MutableSet<T> = this ?: mutableSetOf()
fun <K, V> MutableMap<K, V>?.ensureInit(): MutableMap<K, V> = this ?: mutableMapOf()
