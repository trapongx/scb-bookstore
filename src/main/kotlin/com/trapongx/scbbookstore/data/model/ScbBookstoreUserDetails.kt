package com.trapongx.scbbookstore.data.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class ScbBookstoreUserDetails(user: User) : UserDetails {

    override fun getAuthorities() = listOf<GrantedAuthority>()

    override fun isEnabled() = true

    private val username = user.username!!
    override fun getUsername() = username

    override fun isCredentialsNonExpired() = true

    private val password = user.password!!
    override fun getPassword(): String = password

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true
}