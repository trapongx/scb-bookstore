package com.trapongx.scbbookstore.controller

import com.fasterxml.jackson.annotation.JsonProperty
import com.trapongx.scbbookstore.data.model.Order
import com.trapongx.scbbookstore.data.model.ScbBookstoreUserDetails
import com.trapongx.scbbookstore.data.repo.BookRepository
import com.trapongx.scbbookstore.data.repo.OrderRepository
import com.trapongx.scbbookstore.data.repo.UserRepository
import com.trapongx.util.smartGetMessage
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class OrderController {

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var orderRepository: OrderRepository

    @Autowired
    lateinit var bookRepository: BookRepository

    class CreateRequestBody {
        @JsonProperty("orders")
        var bookIds = listOf<Long>()
    }

    class BookIdNotFoundException(val bookId: Long) : Exception("Book ID $bookId not found")

    @PostMapping("/users/orders")
    fun create(@RequestBody body: CreateRequestBody): ResponseEntity<Map<String, Any?>?> {
        return try {
            val userDetails = SecurityContextHolder.getContext()?.authentication?.principal as? ScbBookstoreUserDetails
                ?: return ResponseEntity(HttpStatus.UNAUTHORIZED)
            val order = Order().also {
                it.user = userRepository.findByUsername(userDetails.username)
                    ?: throw Exception("Error reading user information")
                it.books.putAll(
                    body.bookIds.map { bookId ->
                        val book = bookRepository.findOne(bookId)
                            ?: throw BookIdNotFoundException(bookId)
                        book to book.price!!
                    }
                )
                it.price = it.books.values.sum()
            }
            val savedOrder = orderRepository.save(order)
            ResponseEntity(mapOf("price" to savedOrder.price), HttpStatus.OK)
        } catch (bookIdNotfoundException: BookIdNotFoundException) {
            ResponseEntity(mapOf("error" to bookIdNotfoundException.smartGetMessage()), HttpStatus.BAD_REQUEST)
        } catch (t: Throwable) {
            logger.error(t.smartGetMessage())
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}