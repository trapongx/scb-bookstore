package com.trapongx.scbbookstore

import com.trapongx.scbbookstore.data.service.BookService
import com.trapongx.scbbookstore.data.service.RemoteBookService
import com.trapongx.util.getResource
import org.junit.After
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired

abstract class DefaultAppContextSharedTestWithSampleBooks(mockClock: Boolean = false) : DefaultAppContextSharedTest(mockClock) {

    @Autowired
    lateinit var bookService: BookService

    @Mock
    lateinit var remoteBookUrlProviderMock: RemoteBookService.UrlProvider

    private lateinit var remoteBookUrlProviderSaved: RemoteBookService.UrlProvider

    @Before
    fun applyRemoteBookUrlProviderMock() {
        remoteBookUrlProviderSaved = bookService.remoteBookService.urlProvider
        bookService.remoteBookService.urlProvider = remoteBookUrlProviderMock
    }

    @Before
    fun insertSampleBooks() {
        val packagePath = "com/trapongx/scbbookstore/data/service"

        Mockito.`when`(remoteBookUrlProviderMock.getBooksUrl())
                .thenReturn(getResource("$packagePath/sample-remote-books-1.json"))
        Mockito.`when`(remoteBookUrlProviderMock.getBooksRecommendationUrl())
                .thenReturn(getResource("$packagePath/sample-remote-books-recommendation-1.json"))

        bookService.updateBooks()
    }

    @After
    fun restoreRemoteBookUrlProviderMock() {
        bookService.remoteBookService.urlProvider = remoteBookUrlProviderSaved
    }

}
