package com.trapongx.scbbookstore

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableConfigurationProperties
@EnableScheduling
@EnableJpaAuditing(dateTimeProviderRef = "clockBasedAuditorDateTimeProvider")
class ScbBookstoreApplication

fun main(args: Array<String>) {
    SpringApplication.run(ScbBookstoreApplication::class.java, *args)
}
