package com.trapongx.scbbookstore.time

import org.springframework.stereotype.Service
import java.time.Clock
import java.util.*

@Service
class ClockService {
    var provider: ClockProvider = object : ClockProvider {
        override fun getClock() = Clock.systemDefaultZone()
    }

    fun getClock(): Clock = provider.getClock()

    fun getDate(): Date = Date(getClock().millis())
}