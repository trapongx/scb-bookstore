package com.trapongx.scbbookstore.controller

import com.trapongx.scbbookstore.data.model.ScbBookstoreUserDetails
import com.trapongx.scbbookstore.data.model.User
import com.trapongx.scbbookstore.data.repo.OrderRepository
import com.trapongx.scbbookstore.data.repo.UserRepository
import com.trapongx.util.smartGetMessage
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
class UserController {

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var orderRepository: OrderRepository

    @PostMapping
    fun create(@RequestBody user: User) {
        userRepository.save(user)
    }

    @GetMapping
    fun getLoggedInUserInfo(): ResponseEntity<Map<String, Any?>> {
        val userDetails = SecurityContextHolder.getContext()?.authentication?.principal as? ScbBookstoreUserDetails
            ?: return ResponseEntity(HttpStatus.UNAUTHORIZED)
        return try {
            val user = userRepository.findByUsername(userDetails.username)
                ?: throw Exception("Error reading user information")
            val orders = orderRepository.findAllByUserUsernameOrderById(userDetails.username)
            mapOf(
                "name" to user.name,
                "surname" to user.surname,
                "date_of_birth" to user.dateOfBirth,
                "books" to orders.flatMap { it.books.keys.map { it.id!! } }
            ).let {
                ResponseEntity(it, HttpStatus.OK)
            }
        } catch (t: Throwable) {
            logger.error(t.smartGetMessage())
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping
    fun delete(): ResponseEntity<Unit> {
        val userDetails = SecurityContextHolder.getContext()?.authentication?.principal as? ScbBookstoreUserDetails
            ?: return ResponseEntity(HttpStatus.UNAUTHORIZED)
        return try {
            val user = userRepository.findByUsername(userDetails.username)
                ?: throw Exception("Error reading user information")
            userRepository.delete(user)
            ResponseEntity(HttpStatus.OK)
        } catch (t: Throwable) {
            logger.error(t.smartGetMessage())
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}